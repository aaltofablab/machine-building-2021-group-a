# Filament Winder Station


## Hero shot of the machine
<img src="../pictures/present.JPG">

This machine is being built by the following students.

- Karl Mihhels
- Daniel Wilenius
- Ruo-Xuan Wu

## Inspiration

PLA filaments are the main materials we used to 3D printing, but whenever we change the colors or print out something wrong the leftover couldn't be used anymore. Base on this, we found a [Filabot](https://www.filabot.com/collections/filabot-core/products/filabot-original-ex2) in our storage that ables to recyle the leftover of 3D print. But we need to roll it up to spool **manually**, which might lead to the **uneven thickness**. It might be a good idea if we have a machine to wind the filaments **automatically** and also the **tighten** it.

## Individual contribution

- Mechanism, Machine Design includes CAD/3D modelling, CNC milling and assembling: [Russian Wu](https://russian_wu.gitlab.io/fab-academy-2021/assignments/week10-11-mechanical-and-machine-design/week10-11-mechanical-and-machine-design/)
- Actuation and Automation: [Daniel Wilenius](https://daali98.gitlab.io/danielfablab2021/week_10.html)
- User Interface and Communication: [Karl Mihhels](https://karlpalo.gitlab.io/fablab2021//assignments/assignment-10/)


## Mechanical challenges and Solutions

(Pictures details please look into [Russian's Documentation](https://russian_wu.gitlab.io/fab-academy-2021/assignments/week10-11-mechanical-and-machine-design/week10-11-mechanical-and-machine-design/))

1. This was our first time to use **Luzbot mini**, however the performance wasn't really good. The support was printed failed, which led to the some **falls** on the hanging part. The size wasn't measured correctly, thus it didn't fit our spool. The worst was the printed direction couldn't afford the **horizontal force**.

**!! Solution:**
Changed the **printing direction** and increased the **infill percentage to 80%**.

2. The 3D printer couldn't stick on the heating platform, which led the prints detached and failed printing.

**!! Solution:**
It might because of wrong tools to took off the prints previously which led to the tessalon platform was detroied.
Cleaning the platform gently and use some glue before printing would help the prints to attach on the stage.  


3. The 3D printed result was **shrunk** and the support in screw hole was hard to clean so that the screw couldn't fit in.

**!! Sollution:**
- I created a **cube test** model, in order to know how much it shrunk to add a shrink tolerance for our model.  

- **Rubbing knife** helped me to clean the **screw hole** really well.  

- **A hot air gun, hammer and a piece of cylinder wood** helped me to hammer the screw into our holde properly without any unneccessary hurt.


4. The support of our holder is too complicated to take off after 3D printing.

**!! Sollution:**
The stratagy is that separating a model into two parts and connect it later.


5. The pocket engraved by CNC machine was too small to fit the bearing. Drilling bit wasn't really went through the materials.

**?? Reasons and !! Sollutions:**
The **radius stock to leave** was selected. The drilling function would left less than 1mm on the bottom of stock. To drill through the whole material **Bottom Height offset** is neccessary.

**!! The little tip that saves my time!!:**
If the material allows always locate it at the **END of CNC machine**, which help me to relocate my materials although I already took my material away from it.

6. The bearing and screw wasn't tight so that the station was wobble.

**!! Sollutions:**
**Electric retardant tapes** helped me to fill up the gap between screws bearings.




## Video presentation

Our Machine was in principle functional.

[<img src="../pictures/manual_functionality.jpg">](../Videos/manual_functionality.mp4)

And without belt it responded to GUI commands as expected.

[<img src="../pictures/motor_rotating_nobelt.jpg">](../Videos/motor_rotating_nobelt.mp4)

However, the belt drive proved to be unfunctional, and thus the project can be considered to be an unworking prototype.

[<img src="../pictures/motor_rotating_belt.jpg">](../Videos/motor_rotating_belt.mp4)

## Future Development Opportunities

As stated in the video presentation, the ultimate fate of the machine was to be unfunctional due to the poor functionality of the belt drive. The main issue was that the belt did not produce a stable fix on the motor, especially at the point where the belt was glued together.

This could have been avoided in several ways.
  - Using a direct drive with no belts (e.g. connecting the motor directly to the wheel)
  - Used a ready made belt with a belt tensioner that would have been designed for that particular length of belt.

Furthermore, our machine was completed with only the filament pulling motor having a driver. The filament winder motor, and its automation, and software design were never completed.

## Bill of materials

- Bill of materials (BOM), and for custom parts
- Description on how to repeat the build

**Stepper motor driver and automation how-to**

1. Mill the pcb board, both milling and drilling is required to create this pcb. [Files](../files/stepstick-shield-master)
2. Insert nits in to the holes you drilled to be able to solder it from underneath the board.
3. Solder all the components to the board. Stepper motor driver, leds, resistors, capacitor, power connector, connection for the motor and pins to connect it to your Arduino Uno.
4. Here's how your ready pcb should look.
[<img src="../pictures/4.jpg">](../pictures/4.jpg)

The pcb needs to be run by an Arduino Uno and uses GRBL to control the stepper motor.
So next we install GRBL on our Arduino.

1. Downloading GRBL from their github [Download](https://github.com/grbl/grbl/archive/master.zip)
2. Extract the grbl-master folder
3. Run the Arduino IDE
4. From the application bar menu, choose: Sketch -> #include Library -> Add Library from file.ZIP…
5. Select grbl from the grbl-master folder

Now the GRBL library is installed in your Arduino IDE, next plug in your Arduino to your computer.

1. From File menu click open and select the file grblUpload.ino
2. Click Upload and wait for it to finish

Now that the pcb is ready and Arduino prepared we can test them out and see that everything is working as it should so connect the stepper motor to the pcb and then the pcb to the arduino that you connect to your computer and play around with UGS(Universal Gcode Platform) for a bit.

At this point the system is ready to receive G-Code and act on it.







## Source Code
See the /User_interface/readme.md  for info on the source code of GUI
