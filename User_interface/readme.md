# QtGUI for the filament winding machine

This GUI is meant for arduino that has been initialized with GRBL1.1

The GUI works by running the Interface_final.py via your Python

However on line 70
"    ser = serial.Serial('COM4', 115200)  # open serial port"
You must replace COM4 with whatever port you are using with your arduino

Also the pyserial-folder must be kept in the same folder as interface_final.py, as the interface_final.py uses the functionalities of that module.

This GUI was developed using PyQt5, and is licenced under GPL2



The archive-folder contains development files for the academic intrest of those who want to see the various non-functional iterations of the GUI in the past.
