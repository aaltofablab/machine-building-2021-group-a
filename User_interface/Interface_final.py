###################################################
## Python Backend for machine building assignment for FABACADEMY 2021
##
##
##
##
##
##################################################

#################################################################### MAIN FUNCTIONALITY BELOW


#Rotationspeeds given as deg/s
def initializer_17HM19_2004S():
    initialcommands = ["$$","\r\n","$0 = 10","$1 = 25","$3 = 0","$4 = 0","$5 = 0",
                       "$6 = 0","$10 = 3","$13 = 0","$20 = 0","$21 = 0","$22 = 0",
                       "$23 = 0","$100 = 3200.000","$110 = 180.000","$2 = 0","$120 = 1.000"]
    return print("\n".join(initialcommands))

#Note, speed is always in RPM/s
def winder_CWmove(rounds,RPM):
    primer = "$J=G21G91X"
    spacer = "F"
    rounds = int(rounds)
    rounds = -rounds
    rounds = str(rounds)
    RPM = str(RPM)
    newstr = "".join((primer,rounds,spacer,RPM))
    return(newstr)

def winder_CCWmove(rounds,RPM):
    primer = "$J=G21G91X"
    spacer = "F"
    rounds = int(rounds)
    print(rounds)
    rounds = str(rounds)
    RPM = str(RPM)
    newstr = "".join((primer,rounds,spacer,RPM))
    return(newstr)
    


#   Feed rate override ASCII-commands (can be inputted anytime)
#    0x90 : Set 100% of programmed rate.
#    0x91 : Increase 10%
#    0x92 : Decrease 10%
#    0x93 : Increase 1%
#    0x94 : Decrease 1%
#initialize feedrate for overrides (LIVE ADJUSTMENT --- NOT IMPLEMENTED)
#feedrate = 100
#def setfeednominal():
#    print("0x90")
#    global feedrate
#    feedrate = 100
#def setfeedincrease1():
#    print("0x93")
#    global feedrate
#    feedrate = feedrate + 1
#def setfeeddecrease1():
#    print("0x94")
#    global feedrate
#    feedrate = feedrate - 1
#################################################################### SERIAL CONNECTIVITY BELOW


import serial
import time
def sendcommand_openport():
    global ser
    ser = serial.Serial('COM4', 115200)  # open serial port
    print(ser.name)         # check which port was really used

def sendcommand_wakeup():
    #wake up the board
    ser.write("\r\n\r\n".encode())
    time.sleep(2)   # Wait for grbl to initialize 
    ser.flushInput()  # Flush startup text in serial input
    
def sendcommand(command):
    #insert commands
    if command == None:
        command = " "
    print(command)
    l = command.strip() # Strip all EOL characters for consistency
    print('Sending: ' + l,)
    l = l + '\n'
    ser.write(l.encode()) # Send g-code block to grbl
    grbl_out = ser.readline() # Wait for grbl response with carriage return
    grbl_out = grbl_out.decode()
    print(' : ' + grbl_out.strip())
        

    
def sendcommand_closeport():
    time.sleep(2)
    ser.close()
    
sendcommand_openport()
sendcommand_wakeup()
sendcommand(initializer_17HM19_2004S())
sendcommand_closeport()
#################################################################### INTERFACE AND INTERFACE FUNCTIONS BELOW

import sys

from functools import partial

# Import QApplication and the required widgets from PyQt5.QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QCheckBox
__version__ = "alpha"
__author__ = "Karl"

ERROR_MSG = "ERROR"

# Create a subclass of QMainWindow to setup the calculator's GUI
class winderUi(QMainWindow):
    """winders's View (GUI)."""

    def __init__(self):
        """View initializer."""
        super().__init__()
        # Set some main window's properties
        self.setWindowTitle("Windersoft")
        self.setFixedSize(800, 800)
        # Set the central widget and the general layout
        self.generalLayout = QVBoxLayout()
        
        self._centralWidget = QWidget(self)
        self.setCentralWidget(self._centralWidget)
        self._centralWidget.setLayout(self.generalLayout)
        # Create the display and the buttons
        self._createRPMDisplay()
        self._createRDisplay()
        self._createTickbox()
        self._createButtons()


    def _createRPMDisplay(self):
        """Create the display."""

        # Create the display widget
         

        self.labelRPM_winder = QLabel('This row is for the RPM', self)
        self.displayRPM_winder = QLineEdit()
        # Set some display's properties
        self.displayRPM_winder.setFixedHeight(40)
        #create the dipslaywidget layout
        hbox = QHBoxLayout()
        hbox.addWidget(self.labelRPM_winder)
        hbox.addWidget(self.displayRPM_winder)

        
        # Add the display to the general layout
        self.generalLayout.addLayout(hbox)
        
    def _createRDisplay(self):
        """Create the display."""
        # Create the display widget
        self.labelR_winder = QLabel('This row is for the number of rotations', self)
        self.displayR_winder = QLineEdit()
        # Set some display's properties
        self.displayR_winder.setFixedHeight(40)
        #create the dipslaywidget layout
        hbox = QHBoxLayout()
        hbox.addWidget(self.labelR_winder)
        hbox.addWidget(self.displayR_winder)

        
        # Add the display to the general layout
        self.generalLayout.addLayout(hbox)
        
    def _createTickbox(self):
        self.tickbox_winder = QCheckBox("Tick to set direction to CCW '(default is CW)'")
        self.generalLayout.addWidget(self.tickbox_winder)
        
        
    def _createButtons(self):
        """Create the buttons."""
        self.buttons = {}
        buttonsLayout = QGridLayout()
        # Button text | position on the QGridLayout
        buttons = {
            "Send_to_winder": (1, 0),
            #"8": (2, 0),

        }
        # Create the buttons and add them to the grid layout
        for btnText, pos in buttons.items():
            self.buttons[btnText] = QPushButton(btnText)
            buttonsLayout.addWidget(self.buttons[btnText], pos[0], pos[1])
        # Add buttonsLayout to the general layout
        self.generalLayout.addLayout(buttonsLayout)
        
        
        

    def setRPMReadText(self,lineeditname):
        userinput = self.lineeditname.text()


        
# Create a Model to handle the operation of the software
def evaluateExpression(expression):
    """Evaluate an expression."""
    try:
        result = str(eval(expression, {}, {}))
    except Exception:
        result = ERROR_MSG

    return result

def refresh_values(test):
    print("potato")
    
# Create a Controller class to connect the GUI and the model
class softCtrl:
    #""Control functions of the software""

    def __init__(self, model, view):
        """Controller initializer."""
        self._evaluate = model
        self._view = view
        # Connect signals and slots
        self._connectSignals()

    def _calculateResult(self):
        """Evaluate expressions."""
        result = self._evaluate(expression=self._view.displayRPM.Text())
        self._view.setDisplayText(result)

    def _buildExpression(self, sub_exp):
        """Build expression."""
        if sub_exp == "Send_to_winder":
            print("windersend pressed")
            winderRPM = self._view.displayRPM_winder.text()
            winderRPM = int(winderRPM)
            winderR = self._view.displayR_winder.text()
            winderR = int(winderR)
            winderdirection = self._view.tickbox_winder.isChecked()
            if not winderdirection:
                print("cw")
                sendcommand_openport()
                sendcommand_wakeup()
                sendcommand(winder_CWmove(winderR,winderRPM))
                sendcommand_closeport()
                            
            if winderdirection:
                print("ccw")
                sendcommand_openport()
                sendcommand_wakeup()
                sendcommand(winder_CCWmove(winderR,winderRPM))
                sendcommand_closeport()


       # RPM = self._view.RPMdisplayText()
        #R = self._view.RdisplayText()
       # CCW = self._view.RdisplayText()
      #  expression = self._view.displayText() + sub_exp
      #  self._view.setDisplayText(expression)

    def _connectSignals(self):
        for btnText, btn in self._view.buttons.items():
                btn.clicked.connect(partial(self._buildExpression, btnText))
    # #  """Connect signals and slots."""
      #  for btnText, btn in self._view.buttons.items():
         #   if btnText not in {"=", "C"}:
           #     btn.clicked.connect(partial(self._buildExpression, btnText))



# Client code
def main():
    """Main function."""
    # Create an instance of `QApplication`
    pycalc = QApplication(sys.argv)
    # Show the calculator's GUI
    view = winderUi()
    view.show()
    # Create instances of the model and the controller
    model = evaluateExpression
    softCtrl(model=model, view=view)
    #Testcommands
    print("ilovepotatoes")
    # Execute calculator's main loop
    sys.exit(pycalc.exec_())


if __name__ == "__main__":
    main()